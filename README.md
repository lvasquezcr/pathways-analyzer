# pathways-analyzer

Pathways Analyzer is a prototype to support the design process of metabolic pathways. It suggests metabolic pathways through the application of pathfinding algorithms that establish relevant links between compounds that are essential to relevant processes such as biofuel production.
The tool is a command line application written in Java.  Support tasks were developed in Ruby for the elimination of nested pathways and Perl to obtain complementary information from the GenBank database. Finally, pathways and assemblies are visualized by using Javascript and JUNG libraries, the Java Universal Network/Graph Framework.

## Prerequisites
- Java 1.7.0
- Ruby 1.9.3
- Perl 5.14.2
- KEGGtranslator 2.3 - http://www.cogsys.cs.uni-tuebingen.de/software/KEGGtranslator/
- Runs on Linux :) - Tested on Xubuntu 12.04.2
 

## Demo

In this link you can find a short video of a sample run of the tool: https://www.dropbox.com/s/mt5zcbja5b9ial9/Pathways_Analyzer_Demo.mp4?dl=0