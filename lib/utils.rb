#!/usr/bin/ruby

require 'pathname'
require 'optparse'

class Utils

  def initialize
  end

  def get_pathways_list path
  
    files_list = %x(grep title #{path}/*.xml | sed 's?:.*=?,?g')
    array = files_list.split("\n")
    array.each {|line|
      pathway_info = line.split(",")
      filename = Pathname.new(pathway_info[0]).basename 
      pathway_name = pathway_info[1]
      organism = "#{filename}".slice(0..2)
      puts "#{filename},#{pathway_name},#{organism}"
    }

  end
end

if __FILE__ == $0

  options = {}
  OptionParser.new do |opts|
    opts.banner = "Usage: example.rb [options]"

    opts.on("-d", "--dir DIR", "Pathways .xml files directory") do |dir|
      options[:dir] = dir
    end

  end.parse!


  utils = Utils.new
  utils.get_pathways_list options[:dir]

end
