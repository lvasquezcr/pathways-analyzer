#!/usr/bin/ruby -w

require 'xmlsimple'

class Fixer

  def initialize graph_file
    @graph_file = graph_file
  end
  
  
  # Loads the .xml file so it can be fixed
  def load_xml
  
    # Local variables
    new_node_list = Array.new
    new_edge_list = Array.new
    new_edges_info = Hash.new
  
    # Read .xml file into an object
    config = XmlSimple.xml_in(@graph_file)
  
    # List of first-level nodes of the graph
    node_list = config['graph'][0]['node']
  
    ##################
    ## Fixing Nodes ##
    ##################
  
    # For each node in the list
    node_list.each {|value|
  
      # Get id attribute
      node_id = value['id']
  
      # Get type to filter groups 
      node_type = value['yfiles.foldertype']
    
      # When is a group, extract its nodes
      if(node_type =~ /group/)
  
        # If there entry for this group, initialize it
        if(!new_edges_info.has_key?(node_id))
          new_edges_info[node_id] = Array.new
        end
  
        # Get nodes in the nested graphs 
        second_level_graph = value['graph'][0]['node']
        
        # Move nested nodes to the primary hierarchy
        second_level_graph.each {|node|
          
          # Move new node
          new_node_list << node
  
          # Add info about ids to update later
          new_edges_info[node_id] << node['id']
         }
  
      else
  
        # If the node is not a group, keep is as it is
        new_node_list << value
      end
  
    }
  
    ########################
    ### Fixing the edges ###
    ########################
  
    # Get list edges
    edge_list = config['graph'][0]['edge']
    
    # Get the id of the last edge
    edge_id =  edge_list.last['id'].match(/\d+/)[0].to_i+1
    
    # For each edge
    edge_list.each { |edge|
  
      
      # Get the source and target nodes to check if they should be updated
      source = edge['source']
      target = edge['target']
  
      # Get the dictionaries of nodes to be changed
      source_list = new_edges_info[source]
      target_list = new_edges_info[target]
  
      # Analyze every edge,  update when is required
  
      # Case 1: Both source and target must be updated
      if(new_edges_info.has_key?(source) && new_edges_info.has_key?(target))
  
        # Array to work out the edges
        temp_result = Array.new
  
        # For each new source, replicate and update the edge
        source_list.each { |new_source|
          edge['source'] =  new_source
          temp_result << edge.dup
        }
  
        # For each new target, update the source edge and replicate when required
        temp_result.each {|temp_node|
  
          target_list.each { |new_target| 
            temp_node['target'] = new_target
            temp_node['id'] = edge_id
  
            # When the target is updated, store it to the final result
            new_edge_list << temp_node.dup
            edge_id += 1
            }
        }
     
      # Case 2: Update just the source
      elsif new_edges_info.has_key?(source)
        source_list.each { |new_source|
          edge['source'] =  new_source
          edge['id'] = edge_id
          new_edge_list << edge.dup
          edge_id += 1
        }
  
      # Case 3: Update just the target
      elsif new_edges_info.has_key?(target)
        target_list.each { |new_target|
          edge['target'] =  new_target
          edge['id'] = edge_id
          new_edge_list << edge.dup
          edge_id += 1
      
      # Default: If the edge does not has any node to be updated, just add it
      }else
        new_edge_list << edge
      end
    }
  
    # Substitute all data with the new lists
    config['graph'][0]['edge'] = new_edge_list
    config['graph'][0]['node'] = new_node_list
  
    # Dump the config object into an .xml file
    xml = XmlSimple.xml_out(config)
  
    # Write file
    File.open(@graph_file, 'w') {|f| f.write(xml) }
  
  end

end

if __FILE__ == $0
  fixer = Fixer.new ARGV[0]
  fixer.load_xml
end



