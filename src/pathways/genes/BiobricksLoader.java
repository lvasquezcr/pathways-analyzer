package pathways.genes;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import pathways.datautils.FileUtils;
import pathways.datautils.Log;
import pathways.entities.Biobrick;

public class BiobricksLoader {

	private ArrayList<Biobrick> biobricksList;
	private String partField, partIdField, partNameField, seqDataField;
	private ArrayList<String> categoryField;

	/**
	 * Biobricks Loader constructor
	 * @param filename load biobricks file list
	 */
	public BiobricksLoader(String filename) {

		biobricksList = new ArrayList<Biobrick>();
		categoryField = new ArrayList<String>();
		partField = "";
		partIdField = "";
		partNameField = "";
		seqDataField = "";

	}

	/**
	 * Load Biobricks from file
	 * 
	 * @return true if the loading was successful
	 * @throws IOException
	 */
	public boolean loadBiobricks(String biobricksFilename) throws IOException {

		Log.info("Parsing biobricks file " + biobricksFilename + "..");
		boolean result = false;
		File biobricksFile = new File(biobricksFilename);

		if (biobricksFile.exists()) {

			ArrayList<String> biobricksFiles = FileUtils
					.loadFile(biobricksFilename);

			for (int i = 0; i < biobricksFiles.size(); i++) {

				
				String file = biobricksFiles.get(i);
				Log.info("Loading file "+file+"..");
				parseFileSAX(biobricksFile.getParent() + "/" + file);
			}

			result = true;

		}
		return result;

	}

	/**
	 * Parse .xml with SAX (Simple API for xml)
	 * 
	 * @param xmlFile
	 *            input filename
	 */
	public void parseFileSAX(String xmlFile) {
		try {

			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();

			DefaultHandler handler = new DefaultHandler() {
				boolean part_id = false;
				boolean part_name = false;
				boolean seq_data = false;
				boolean category = false;

				public void startElement(String uri, String localName,
						String qName, Attributes attributes)
						throws SAXException {

					if (qName.equalsIgnoreCase("PART_ID")) {
						part_id = true;
					}

					if (qName.equalsIgnoreCase("PART_NAME")) {
						part_name = true;
					}

					if (qName.equalsIgnoreCase("SEQ_DATA")) {
						seq_data = true;
					}

					if (qName.equalsIgnoreCase("CATEGORY")) {
						category = true;
					}

				}

				public void endElement(String uri, String localName,
						String qName) throws SAXException {

					if (qName.equalsIgnoreCase("PART")) {
						biobricksList.add(new Biobrick(partField, partIdField,
								partNameField, seqDataField, categoryField));
						categoryField = new ArrayList<String>();
						partField = "";
						partIdField = "";
						partNameField = "";
						seqDataField = "";

					}

				}

				public void characters(char ch[], int start, int length)
						throws SAXException {

					if (part_id) {
						partIdField = new String(ch, start, length);
						part_id = false;
					}

					if (part_name) {
						partNameField = new String(ch, start, length);
						part_name = false;
					}

					if (seq_data) {
						seqDataField = new String(ch, start, length);
						seq_data = false;
					}

					if (category) {

						categoryField.add(new String(ch, start, length));
						category = false;
					}

				}

			};

			saxParser.parse(xmlFile, handler);
			printBiobricks();

		} catch (Exception e) {
			Log.error("There was an error parsing the biobricks list");
			Log.error("Details:" + e.getMessage());
		}
	}

	/**
	 * Print Biobricks data structure
	 */
	public void printBiobricks() {
		for (int i = 0; i < biobricksList.size(); i++) {

			Biobrick biobrick = biobricksList.get(i);
			Log.debug("Part_id: " + biobrick.getPart_id());
			Log.debug("Part_name: " + biobrick.getPart_name());
			Log.debug("Seq_data: " + biobrick.getSeq_data());

			ArrayList<String> categories = biobrick.getCategory();
			for (int j = 0; j < categories.size(); j++) {
				Log.debug("Category: " + categories.get(j));
			}

			Log.debug("");

		}

	}

}
