package pathways.genes;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pathways.config.ConfigData;
import pathways.datautils.ExternalCmdsManager;
import pathways.datautils.FileUtils;
import pathways.datautils.Log;
import pathways.entities.ExecResult;

public class QueryGenBank {

	public QueryGenBank() {

	}

	/**
	 * Get the CDS of the input gene
	 * 
	 * @param cds Genbank code to extract CDS
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public ArrayList<String> getGeneCDS(String cds) throws IOException,
			InterruptedException {

		if (!new File(ConfigData.cdsDir + cds + ".cds.data").exists()) {

			Log.info("Querying GenBank..");
			List<String> cmd = Arrays.asList(ConfigData.queryGenBankScript, cds);

			ExecResult result = ExternalCmdsManager.exec(cmd);
			
			if(result.isFailed())
			{
				Log.error("Failed to load CDS for "+cds);
				Log.error("Details:");
				Log.error(result.getError());
			}
		}

		return FileUtils.loadFile(ConfigData.cdsDir + cds + ".cds.data");
	}

}
