package pathways.datautils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class FileUtils {

	/***
	 * Load the content of the file
	 * 
	 * @param file
	 *            name of the file
	 * @return content of the file into an ArrayList
	 * @throws IOException
	 */
	public static ArrayList<String> loadFile(String file) throws IOException {

		ArrayList<String> files = new ArrayList<String>();
		String line = "";

		BufferedReader reader = new BufferedReader(new FileReader(
				new File(file)));

		while ((line = reader.readLine()) != null) {
			files.add(line);
		}

		reader.close();

		return files;
	}

	/***
	 * Load list of files
	 * 
	 * @param file
	 *            input filename with list of pathways
	 * @return
	 * @throws IOException
	 */
	public static ArrayList<String> loadFilesList(String file)
			throws IOException {

		Log.info("Loading list of files:" + file);

		ArrayList<String> files = new ArrayList<String>();

		if (new File(file).exists()) {

			String line = "";

			BufferedReader reader = new BufferedReader(new FileReader(new File(
					file)));

			while ((line = reader.readLine()) != null) {
				files.add(line);
			}

			reader.close();
		} else {
			Log.error("File " + file + " does not exist.");
		}

		return files;
	}

}
