package pathways.datautils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import pathways.config.ConfigData;

public class Log {

	private static Logger LOGGER = Logger.getLogger("PathwaysAnalyzer.log");

	/**
	 * Write informative messages in the logfile
	 * @param info content to write
	 */
	public static void info(String info) {

		LOGGER.info(info);
	}

	/***
	 * Write warning messages in the logfile
	 * @param warning content to write
	 */
	public static void warning(String warning) {
		LOGGER.warning(warning);
	}

	/***
	 * Write error messages in the logfile
	 * @param error content to write
	 */
	public static void error(String error) {
		LOGGER.fine(error);
	}

	/***
	 * Write error messages in the logfile
	 * @param error content to write
	 */
	public static void error(ArrayList<String> error) {

		for (String line : error) {
			LOGGER.fine(line);

		}

	}
	
	/***
	 * Write debug messages in the logfile
	 * @param debug content to write
	 */
	public static void debug(String debug) {
		
		if(ConfigData.DEBUG)
			LOGGER.info(debug);

	}

	/**
	 * Setup the log file with format and other params
	 * @throws SecurityException
	 * @throws IOException
	 */
	static public void setup() throws SecurityException, IOException {

		FileHandler file = new FileHandler("PathwaysAnalyzer.log");
		LOGGER.setLevel(Level.ALL);
		LOGGER.setUseParentHandlers(false);

		file.setFormatter(new Formatter() {
			public String format(LogRecord record) {

				String date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
						.format(new Date(record.getMillis()));

				return record.getLevel() + " : " + date + " - "
						+ record.getMessage() + "\n";
			}
		});

		LOGGER.addHandler(file);

	}


}
