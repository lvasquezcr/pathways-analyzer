package pathways.datautils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import pathways.entities.ExecResult;
import cern.colt.Arrays;

public class ExternalCmdsManager {

	/***
	 * 
	 * @param cmd
	 *            command to execute
	 * @return result with the command output and error
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static ExecResult exec(List<String> cmd) throws IOException,
			InterruptedException {

		ExecResult result;

		Log.info("Executing "+ Arrays.toString(cmd.toArray()) + "..");
		Process process = new ProcessBuilder(cmd).start();
		process.waitFor();

		String line = "";

		ArrayList<String> output = new ArrayList<String>();
		ArrayList<String> error = new ArrayList<String>();

		BufferedReader outputReader = new BufferedReader(new InputStreamReader(
				process.getInputStream()));
		BufferedReader errorReader = new BufferedReader(new InputStreamReader(
				process.getErrorStream()));

		while ((line = outputReader.readLine()) != null) {
			output.add(line);
			System.out.println(line);
		}

		while ((line = errorReader.readLine()) != null) {
			error.add(line);
			System.out.println(line);
		}

		outputReader.close();
		errorReader.close();

		int exitCode = process.exitValue();
		boolean failed = exitCode != 0 ? true : false;

		Log.info("Process finished with exit code: " + process.exitValue());

		result = new ExecResult(failed, output, error, exitCode);

		return result;

	}

}
