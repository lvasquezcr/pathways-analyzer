package pathways.protein;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

import pathways.datautils.Log;

/*
 * http://www.uniprot.org/faq/28#batch_retrieval_perl_example
 * To use our database identifier mapping service programmatically you need to know the abbreviations for the database names. Some databases map only one way.
 * Also: http://www.ebi.ac.uk/uniprot/remotingAPI/ remoting API?
 */
public class UniprotRest {

	private static final String UNIPROT_SERVER = "http://www.uniprot.org/";

	/**
	 * Query UNIPROT server
	 * @param tool name of the tool to query
	 * @param params parameters of the query
	 * @return array with output requested
	 * @throws Exception
	 */
	private static ArrayList<String> run(String tool, ParameterNameValue[] params)
			throws Exception {
		
		ArrayList<String> genesList = new ArrayList<String>();
		
		StringBuilder locationBuilder = new StringBuilder(UNIPROT_SERVER + tool
				+ "/?");
		for (int i = 0; i < params.length; i++) {
			if (i > 0)
				locationBuilder.append('&');
			locationBuilder.append(params[i].name).append('=')
					.append(params[i].value);
		}
		
		String location = locationBuilder.toString();

		URL url = new URL(location);
		Log.info("Submitting to UniprotRest...");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		HttpURLConnection.setFollowRedirects(true);
		conn.setDoInput(true);
		conn.connect();

		int status = conn.getResponseCode();
		
		while (true) {
			int wait = 0;
			String header = conn.getHeaderField("Retry-After");
			if (header != null)
				wait = Integer.valueOf(header);
			if (wait == 0)
				break;
			Log.info("Waiting (" + wait + ")...");
			conn.disconnect();
			Thread.sleep(wait * 1000);
			conn = (HttpURLConnection) new URL(location).openConnection();
			conn.setDoInput(true);
			conn.connect();
			status = conn.getResponseCode();
		}
		if (status == HttpURLConnection.HTTP_OK) {
			Log.info("Got a OK reply");
			InputStream reader = conn.getInputStream();
			URLConnection.guessContentTypeFromStream(reader);
			StringBuilder builder = new StringBuilder();
			int a = 0;
			while ((a = reader.read()) != -1) {
				builder.append((char) a);
			}
			
			String[] data = builder.toString().split("\n");

			for (int i = 0; i < data.length; i++) {
				genesList.add(data[i]);
			}
			
		} else
			Log.error("Failed, got " + conn.getResponseMessage() + " for "
					+ location);
		conn.disconnect();
		
		return genesList;
	}
	
	public static ArrayList<String> getGenes(String proteins) throws UnsupportedEncodingException, Exception
	{
		return run("uniprot", new ParameterNameValue[] {
				
				new ParameterNameValue("columns", "genes,sequence,protein names,entry name,domain,features,length,organism,reviewed,database(EMBL)"),
				new ParameterNameValue("format", "tab"),
				new ParameterNameValue("query",
						proteins), });
	}

	private static class ParameterNameValue {
		
		private final String name;
		private final String value;

		public ParameterNameValue(String name, String value)
				throws UnsupportedEncodingException {
			this.name = URLEncoder.encode(name, "UTF-8");
			this.value = URLEncoder.encode(value, "UTF-8");
		}
	}
}
