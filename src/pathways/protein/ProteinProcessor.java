package pathways.protein;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map.Entry;

import pathways.datautils.Log;
import pathways.entities.Gene;
import pathways.entities.Protein;
import pathways.genes.QueryGenBank;

public class ProteinProcessor {

	Hashtable<String, Protein> proteins;
	Hashtable<String, Gene> genes;

	public ProteinProcessor() {
		proteins = new Hashtable<String, Protein>();
		genes = new Hashtable<String, Gene>();
	}

	/**
	 * Request protein information based on its Id
	 * @param protein name of the protein
	 * @throws UnsupportedEncodingException
	 * @throws Exception
	 */
	public void requestProteinsfromId(String protein)
			throws UnsupportedEncodingException, Exception {
		String proteinKey = protein.replaceAll("\\s*,\\s*|\\s+", " || ");
		Log.info("Searching the following query: " + proteinKey
				+ "..");
		Log.info("Connecting to UNIPROT services..");
		extractProteins(UniprotRest.getGenes(proteinKey));
	}

	/**
	 * Extract information obtained from each query
	 * @param proteinsList list of proteins information
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void extractProteins(ArrayList<String> proteinsList)
			throws IOException, InterruptedException {

		// System.out.println("Size:" + genesDna.size());
		if (proteinsList.size() > 1) {

			for (int i = 1; i < proteinsList.size(); i++) {

				String gene = proteinsList.get(i);

				String[] data = gene.split("\t");

				Protein protein = new Protein(data[0], data[1], data[2],
						data[3], data[4], data[5], data[6], data[7], data[8],
						data[9]);
				String key = data[3];

				String crossRef = data[9];
			
				/* Don't process proteins that already exist */
				if (!proteins.containsKey(key)) {
					protein.setGenesList(createListGenes(crossRef));
					proteins.put(key, protein);
				}
			}
		} else {
			Log.warning("No proteins were found.");
		}

		printGenes();
		Log.info("Proteins queries completed.");
	}

	public ArrayList<Gene> createListGenes(String crossRef) throws IOException,
			InterruptedException {

		ArrayList<Gene> cdsList = new ArrayList<Gene>();

		String[] data = crossRef.split(";");

		/* Get CDS from GenBank */
		QueryGenBank query = new QueryGenBank();

		for (int i = 0; i < data.length; i++) {
			String geneKey = data[i];

			/* Don't process genes that already exist */
			if (!genes.containsKey(geneKey)) {

				// Check if the file already exist
				Gene gene = new Gene(geneKey, query.getGeneCDS(data[i]));

				Log.info("Loaded cross-reference into gene for: " + gene.getName());

				genes.put(geneKey, gene);
				cdsList.add(gene);
			} else {
				cdsList.add(genes.get(geneKey));
			}
		}

		return cdsList;

	}

	public void printGenes() {
		Log.debug("Printing genes..");
		for (Entry<String, Protein> entry : proteins.entrySet()) {
			String key = entry.getKey();
			Protein value = entry.getValue();
			Log.debug("Key: " + key);
			Log.debug("Gene: " + value.getGeneNames());
			Log.debug("Protein name: " + value.getProteinNames());
			Log.debug("Sequence: " + value.getDnaSequence());
			Log.debug("Entry Names: " + value.getEntryName());
			Log.debug("Domain: " + value.getDomains());
			Log.debug("Features: " + value.getFeatures());
			Log.debug("Length: " + value.getLength());
			Log.debug("Organism: " + value.getOrganism());
			Log.debug("Reviewed: " + value.getReviewed());
			Log.debug("Cross-reference: " + value.getEmbl_reference());
			Log.debug("");
		}
	}

}
