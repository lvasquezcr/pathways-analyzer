package pathways.entities;

import java.util.ArrayList;

public class Biobrick {

	private String part;
	private String part_id;
	private String part_name;
	private String seq_data;
	private ArrayList<String> category;

	/**
	 * @param part
	 * @param part_id
	 * @param part_name
	 * @param seq_data
	 * @param category
	 */
	public Biobrick(String part, String part_id, String part_name,
			String seq_data, ArrayList<String> category) {
		super();
		this.part = part;
		this.part_id = part_id;
		this.part_name = part_name;
		this.seq_data = seq_data;
		this.category = category;
	}

	/**
	 * @return the part
	 */
	public String getPart() {
		return part;
	}

	/**
	 * @param part
	 *            the part to set
	 */
	public void setPart(String part) {
		this.part = part;
	}

	/**
	 * @return the part_id
	 */
	public String getPart_id() {
		return part_id;
	}

	/**
	 * @param part_id
	 *            the part_id to set
	 */
	public void setPart_id(String part_id) {
		this.part_id = part_id;
	}

	/**
	 * @return the part_name
	 */
	public String getPart_name() {
		return part_name;
	}

	/**
	 * @param part_name
	 *            the part_name to set
	 */
	public void setPart_name(String part_name) {
		this.part_name = part_name;
	}

	/**
	 * @return the seq_data
	 */
	public String getSeq_data() {
		return seq_data;
	}

	/**
	 * @param seq_data
	 *            the seq_data to set
	 */
	public void setSeq_data(String seq_data) {
		this.seq_data = seq_data;
	}

	/**
	 * @return the category
	 */
	public ArrayList<String> getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(ArrayList<String> category) {
		this.category = category;
	}

}
