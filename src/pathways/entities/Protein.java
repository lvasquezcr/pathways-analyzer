package pathways.entities;

import java.util.ArrayList;

public class Protein {

	private String geneNames;
	private String dnaSequence;
	private String proteinNames;
	private String entryName;
	private String domains;
	private String features;
	private String length;
	private String organism;
	private String reviewed;
	private String embl_reference;
	private ArrayList<Gene> genesList;

	/***
	 * @param proteinName genes[0] Uniprot column
	 * @param dnaSequence sequence[1] Uniprot column
	 * @param proteinNames protein names[2] Uniprot column
	 * @param entryName entry name [3] Uniprot column
	 * @param domains domains[4] Uniprot column
	 * @param features features[5] Uniprot column
	 * @param length length[6] Uniprot column
	 * @param organism organism[7] Uniprot column
	 * @param reviewed reviewed[8] Uniprot column
	 * @param embl_reference cross reference[9] Uniprot column
	 */
	public Protein(String proteinName, String dnaSequence, String proteinNames,
			String entryName, String domains, String features, String length,
			String organism, String reviewed, String embl_reference) {
		super();
		this.geneNames = proteinName;
		this.dnaSequence = dnaSequence;
		this.proteinNames = proteinNames;
		this.entryName = entryName;
		this.domains = domains;
		this.features = features;
		this.length = length;
		this.organism = organism;
		this.reviewed = reviewed;
		this.embl_reference = embl_reference;
	}

	/**
	 * @return the proteinName
	 */
	public String getGeneNames() {
		return geneNames;
	}

	/**
	 * @param proteinName
	 *            the proteinName to set
	 */
	public void setGeneNames(String geneNames) {
		this.geneNames = geneNames;
	}

	/**
	 * @return the dnaSequence
	 */
	public String getDnaSequence() {
		return dnaSequence;
	}

	/**
	 * @param dnaSequence
	 *            the dnaSequence to set
	 */
	public void setDnaSequence(String dnaSequence) {
		this.dnaSequence = dnaSequence;
	}

	/**
	 * @return the proteinNames
	 */
	public String getProteinNames() {
		return proteinNames;
	}

	/**
	 * @param proteinNames
	 *            the proteinNames to set
	 */
	public void setProteinNames(String proteinNames) {
		this.proteinNames = proteinNames;
	}

	/**
	 * @return the entryName
	 */
	public String getEntryName() {
		return entryName;
	}

	/**
	 * @param entryName
	 *            the entryName to set
	 */
	public void setEntryName(String entryName) {
		this.entryName = entryName;
	}

	/**
	 * @return the domains
	 */
	public String getDomains() {
		return domains;
	}

	/**
	 * @param domains
	 *            the domains to set
	 */
	public void setDomains(String domains) {
		this.domains = domains;
	}

	/**
	 * @return the features
	 */
	public String getFeatures() {
		return features;
	}

	/**
	 * @param features
	 *            the features to set
	 */
	public void setFeatures(String features) {
		this.features = features;
	}

	/**
	 * @return the length
	 */
	public String getLength() {
		return length;
	}

	/**
	 * @param length
	 *            the length to set
	 */
	public void setLength(String length) {
		this.length = length;
	}

	/**
	 * @return the organism
	 */
	public String getOrganism() {
		return organism;
	}

	/**
	 * @param organism
	 *            the organism to set
	 */
	public void setOrganism(String organism) {
		this.organism = organism;
	}

	/**
	 * @return the reviewed
	 */
	public String getReviewed() {
		return reviewed;
	}

	/**
	 * @param reviewed
	 *            the reviewed to set
	 */
	public void setReviewed(String reviewed) {
		this.reviewed = reviewed;
	}

	/**
	 * @return the embl_reference
	 */
	public String getEmbl_reference() {
		return embl_reference;
	}

	/**
	 * @param embl_reference
	 *            the embl_reference to set
	 */
	public void setEmbl_reference(String embl_reference) {
		this.embl_reference = embl_reference;
	}

	/**
	 * @return the genes_list
	 */
	public ArrayList<Gene> getGenesList() {
		return genesList;
	}

	/**
	 * @param genes_list
	 *            the genes_list to set
	 */
	public void setGenesList(ArrayList<Gene> genes_list) {
		this.genesList = genes_list;
	}

}
