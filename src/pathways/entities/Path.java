package pathways.entities;

import java.util.ArrayList;
import java.util.Hashtable;

import com.tinkerpop.blueprints.Vertex;

public class Path {

	private ArrayList<Vertex> nodes;
	//private Hashtable<String, String> nodesMap;
	//private String description;

	/**
	 * @param nodes
	 * @param nodesMap
	 */
	public Path(Vertex vertex) {
		super();
		this.nodes = new ArrayList<Vertex>();
		//this.nodesMap = new Hashtable<>();
		this.nodes.add(vertex);
		//nodesMap.put(vertex.getId().toString(), "");

	}

	public Path(Path path) {

		ArrayList<Vertex> vertices = path.getNodes();
		this.nodes = new ArrayList<Vertex>();
		//this.nodesMap = new Hashtable<>();

		for (Vertex vertex : vertices) {
			this.nodes.add(vertex);
			//nodesMap.put(vertex.getId().toString(), "");
		}

	}

	public boolean contains(Vertex vertex) {

		//return nodesMap.containsKey(vertex.getId().toString());
		return nodes.contains(vertex);
	}

	public boolean add(Vertex vertex) {

		boolean result = false;

		if (!contains(vertex)) {

			nodes.add(vertex);
			//nodesMap.put(vertex.getId().toString(), "");
			result = true;
		}

		return result;
	}

	public int size() {
		return nodes.size();
	}

	public Vertex getLast() {
		return nodes.get(nodes.size() - 1);
	}

	public void print() {
		//System.out.println(description);
		for (Vertex list : nodes) {
			System.out.print(list + "-");
		}

		System.out.println("");
		System.out.println("");
	}

	/**
	 * @return the nodes
	 */
	public ArrayList<Vertex> getNodes() {
		return nodes;
	}

	/**
	 * @param nodes
	 *            the nodes to set
	 */
	public void setNodes(ArrayList<Vertex> nodes) {
		this.nodes = nodes;
	}

	/**
	 * @return the nodesMap
	 *//*
	public Hashtable<String, String> getNodesMap() {
		return nodesMap;
	}
*/
	/**
	 * @param nodesMap
	 *            the nodesMap to set
	 *//*
	public void setNodesMap(Hashtable<String, String> nodesMap) {
		this.nodesMap = nodesMap;
	}*/
/*
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}*/

}
