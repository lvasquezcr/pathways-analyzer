package pathways.entities;

import java.util.ArrayList;

public class Gene {

	private String name;
	private ArrayList<String> cds_list;

	/**
	 * @param name
	 * @param cds_list
	 */
	public Gene(String name, ArrayList<String> cds_list) {
		super();
		this.name = name;
		this.cds_list = cds_list;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the cds_list
	 */
	public ArrayList<String> getCds_list() {
		return cds_list;
	}

	/**
	 * @param cds_list
	 *            the cds_list to set
	 */
	public void setCds_list(ArrayList<String> cds_list) {
		this.cds_list = cds_list;
	}

}
