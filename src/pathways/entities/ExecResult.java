package pathways.entities;

import java.util.ArrayList;

public class ExecResult {
	
	private boolean failed;
	private ArrayList<String> output;
	private ArrayList<String> error;
	private int exitCode;
	/**
	 * @param failed
	 * @param output
	 * @param error
	 * @param exitCode
	 */
	public ExecResult(boolean failed, ArrayList<String> output,
			ArrayList<String> error, int exitCode) {
		super();
		this.failed = failed;
		this.output = output;
		this.error = error;
		this.exitCode = exitCode;
	}
	/**
	 * @return the failed
	 */
	public boolean isFailed() {
		return failed;
	}
	/**
	 * @param failed the failed to set
	 */
	public void setFailed(boolean failed) {
		this.failed = failed;
	}
	/**
	 * @return the output
	 */
	public ArrayList<String> getOutput() {
		return output;
	}
	/**
	 * @param output the output to set
	 */
	public void setOutput(ArrayList<String> output) {
		this.output = output;
	}
	/**
	 * @return the error
	 */
	public ArrayList<String> getError() {
		return error;
	}
	/**
	 * @param error the error to set
	 */
	public void setError(ArrayList<String> error) {
		this.error = error;
	}
	/**
	 * @return the exitCode
	 */
	public int getExitCode() {
		return exitCode;
	}
	/**
	 * @param exitCode the exitCode to set
	 */
	public void setExitCode(int exitCode) {
		this.exitCode = exitCode;
	}
	
	
	
	

}
