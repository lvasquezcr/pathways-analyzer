package pathways.translator;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import pathways.datautils.ExternalCmdsManager;
import pathways.datautils.Log;
import pathways.entities.ExecResult;
import de.zbit.kegg.Translator;
import de.zbit.kegg.io.KEGGtranslatorIOOptions;

public class TranslateKGML {

	public TranslateKGML() {
	}

	// As a library:
	// http://www.ra.cs.uni-tuebingen.de/software/KEGGtranslator/doc/Javadoc2.0/
	public boolean translate2GraphML(String inFile) throws IOException,
			InterruptedException {

		String name = new File(inFile).getName().replace(".xml", "");
		boolean directory = false;
		List<String> cmds = null;

		/***
		 * KEGG Translator options
		 * 
		 * -ro, --remove-orphans: If true, remove all nodes that have no edges
		 * before translating the pathway. Default: false 
		 * 
		 * -ar<Boolean>,--autocomplete-reactions[ |=]<Boolean> If true, automatically looks
		 * for missing reactants and enzymes of reactions and adds them to the
		 * document. Default: true 
		 * 
		 * --log-level[ |=]<String> Change the log-level
		 * of this application. Default: INFO (ALL, CONFIG, FINE, FINER, FINEST,
		 * INFO, OFF, SEVERE, und WARNING) 
		 * 
		 * -nowhite, --remove-white-gene-nodes
		 * If true, removes all white gene-nodes in the KEGG document (usually
		 * enzymes that have no real instance on the current organism). Default:
		 * false 
		 * 
		 * --gene-names[ |=]<NODE_NAMING> For one KEGG object, multiple
		 * names are available. Choose how to assign one name to this object.
		 * All possible values of <NODE_NAMING> are: SHORTEST_NAME,
		 * FIRST_NAME_FROM_KGML, FIRST_NAME, ALL_FIRST_NAMES,
		 * INTELLIGENT_WITH_EC_NUMBERS, und INTELLIGENT. Default: INTELLIGENT
		 * 
		 * -nopr, --remove-pathway-references If true, removes all entries
		 * (nodes, species, etc.) referring to other pathways. Default: false
		 * 
		 */

		cmds = Arrays.asList("java", "-jar",
				"lib/KEGGtranslatorV2.3.0/KEGGtranslatorV2.3.0.jar", "--input",
				inFile, "--output", "data/" + name + ".graphml", "--format",
				"GraphML", "-arfalse", "-rotrue", "--gene-names=INTELLIGENT_WITH_EC_NUMBERS");

		if (directory) {
			cmds = Arrays.asList("java", "-jar",
					"lib/KEGGtranslatorV2.3.0/KEGGtranslatorV2.3.0.jar",
					"--input", inFile,"--format", "GraphML");

		}

		ExecResult result = ExternalCmdsManager.exec(cmds);

		if (result.isFailed()) {
			Log.error("KEGGtranslator failed in the translation of " + inFile);
			Log.error("Details: ");
			Log.error(result.getError());
		}

		return result.isFailed();

	}

	public void innerTranslate(String in, String out) throws Exception {
		// Fast translation of a KGML to a GraphML file
		Translator.translate(KEGGtranslatorIOOptions.Format.GraphML, in, out);

	}
}
