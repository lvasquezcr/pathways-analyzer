package pathways.main;

import java.io.IOException;
import java.util.ArrayList;

import pathways.config.ConfigData;
import pathways.datautils.FileUtils;
import pathways.datautils.Log;
import pathways.genes.BiobricksLoader;
import pathways.graph.GraphIO;
import pathways.graph.HyperGraph;
import pathways.graph.NavigateGraph;
import pathways.protein.ProteinProcessor;

import com.tinkerpop.blueprints.Vertex;

public class RunAnalyzer {

	private String startNode = "cpd:C00103";
	private String endNode = "cpd:C05345";

	public RunAnalyzer() {

	}

	public void start() {

		/*
		 * TODO: Create required directories TODO: Disable KEGG Translator
		 * splash
		 */

		// Graphs: Tinkerpop - Blueprints
		GraphIO graph = new GraphIO();

		// Pathways: KEGG
		NavigateGraph navigator = new NavigateGraph();

		// Proteins: UNIPROT and GENBANK
		ProteinProcessor proteinsProcessor = new ProteinProcessor();

		// Biobricks: iGEM
		BiobricksLoader biobricksLoader = new BiobricksLoader(
				ConfigData.biobricksFile);

		try {

			/* ====> PATHWAYS <==== */

			// Load pathways from list
			ArrayList<String> pathways = FileUtils
					.loadFilesList(ConfigData.pathwaysFile);

			// Translate input graphs and merge them into a hypergraph
			HyperGraph hypergraph = graph.translateGraphs(pathways);

			// Find a path between two compounds
			//navigator.findPaths(startNode, endNode, hypergraph);
			//navigator.travelGraph(startNode, endNode, hypergraph.getHyperGraph(),14);
			//navigator.travelGraph(startNode, endNode, hypergraph.getHyperGraph(),15);
			//navigator.travelGraph(startNode, endNode, hypergraph.getHyperGraph(),16);
			navigator.travelGraph(startNode, endNode, hypergraph.getHyperGraph(),8);


			/* ====> PROTEINS <==== 

			// Get proteins from path
			ArrayList<Vertex> path = navigator.getProteinsPath();
			String proteins = "";

			// Build a query will all the proteins
			for (Vertex vertex : path) {
				proteins += vertex.getProperty("uniprotIds") + " ";
			}
			proteins = proteins.trim();

			// Query UNIPROT for each protein
			proteinsProcessor.requestProteinsfromId(proteins);
*/
			/* ====> BIOBRICKS <==== 

			// Load available Biobricks
			if (!biobricksLoader.loadBiobricks(ConfigData.biobricksFile))
				System.out
						.println("ERROR: There was an issue loading the biobricks");
			*/
			Log.info("Analysis Completed.");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
