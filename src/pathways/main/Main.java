package pathways.main;

import java.io.IOException;

import pathways.datautils.Log;

public class Main {

	/**
	 * Main Class
	 * @param args
	 */
	public static void main(String[] args) {

		System.out.println("Launching Pathways Analyzer..");
		try {
			Log.setup();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		RunAnalyzer analyzer = new RunAnalyzer();
		analyzer.start();
		System.out.println("Done! :)");

	}

}
