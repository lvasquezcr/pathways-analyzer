package pathways.graph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import pathways.datautils.Log;
import pathways.entities.Path;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.tg.TinkerGraph;
import com.tinkerpop.blueprints.oupls.jung.GraphJung;

import edu.uci.ics.jung.algorithms.shortestpath.DijkstraShortestPath;
import edu.uci.ics.jung.algorithms.shortestpath.UnweightedShortestPath;

public class NavigateGraph {

	private ArrayList<Vertex> nodesPath;
	private ArrayList<Vertex> proteinsPath;

	public NavigateGraph() {

	}

	/***
	 * Get the shortest path between two nodes in a graph
	 * 
	 * @param source
	 *            start node
	 * @param destination
	 *            end node
	 * @param hyperGraph
	 *            graph with all the translated pathways
	 */
	public void findPaths(String source, String destination,
			HyperGraph hyperGraph) {

		nodesPath = new ArrayList<Vertex>();
		proteinsPath = new ArrayList<Vertex>();

		Log.info("Calculating shortest path from " + source + " to "
				+ destination + "..");

		NavigateGraph navigator = new NavigateGraph();
		nodesPath = navigator.runningDijksta(hyperGraph, source, destination);

		for (Vertex vertex : nodesPath) {

			if (vertex.getProperty("type").equals("protein")) {
				proteinsPath.add(vertex);
			}

		}
	}

	public void seeGraphs(Graph graph) {

		VisualizeGraph visualization = new VisualizeGraph();
		// visualization.showGraph(hyperGraph.getHyperGraph());
		visualization.showGraph(graph);
	}

	/*
	 * public void findAllPaths() { NavigateGraph navigator = new
	 * NavigateGraph(); // navigator.travelGraph(hyperGraph.getHyperGraph(),
	 * "cpd:C00103"); navigator.travelGraph(hyperGraph.getHyperGraph(),
	 * "cpd:C00074"); }
	 */

	/**
	 * Get shortest path based on Dijkstra
	 * 
	 * @param hyperGraph
	 *            graph to travel
	 * @param source
	 *            start node
	 * @param destination
	 *            end node
	 * @return
	 */
	public ArrayList<Vertex> runningDijksta(HyperGraph hyperGraph,
			String source, String destination) {

		Log.info("Running algorithm to get shortest path..");
		ArrayList<Vertex> result = new ArrayList<Vertex>();
		Graph inputGraph = hyperGraph.getHyperGraph();
		GraphJung<Graph> graph = new GraphJung<Graph>(inputGraph);

		/*
		 * http://jung.sourceforge.net/doc/api/edu/uci/ics/jung/algorithms/
		 * shortestpath/UnweightedShortestPath.html
		 * http://www.genome.jp/kegg-bin/show_pathway?eco00010
		 */
		DijkstraShortestPath<Vertex, Edge> alg = new DijkstraShortestPath<Vertex, Edge>(
				graph);

		UnweightedShortestPath<Vertex, Edge> bfs_algorithm = new UnweightedShortestPath<Vertex, Edge>(
				graph);

		Vertex start = getVertex(source, graph);

		Vertex end = getVertex(destination, graph);

		if (start == null)
			Log.error("The source node '" + start + "' does not exist.");
		else if (end == null)
			Log.error("The destination node '" + end + "' does not exist.");
		else {

			List<Edge> edges_list = alg.getPath(start, end);
			Number distance = bfs_algorithm.getDistance(start, end);

			Log.info("The shortest unweighted path from " + start + " to "
					+ end + " is:");
			// System.out.println(l.toString());

			result.add(start);

			Log.info("Path length:" + distance);
			Log.info("Number of paths analyzed: "
					+ hyperGraph.getGraphList().size());

			String path = "";
			path = "Start->" + start.getProperty("nodeName") + "->";
			for (int i = 0; i < edges_list.size() - 1; i++) {

				Vertex vertex = edges_list.get(i).getVertex(Direction.IN);
				String label = vertex.getProperty("nodeName");
				String id = vertex.getProperty("keggIds");

				if (label == null) {
					label = id;
				}

				path += label
						+ "("
						+ hyperGraph.getGraphIdsDictionary().get(
								vertex.getProperty("parent")) + ")" + "->";
				result.add(vertex);
			}
			result.add(end);
			path += end.getProperty("nodeName")
					+ "("
					+ hyperGraph.getGraphIdsDictionary().get(
							end.getProperty("parent")) + ")" + "->End";
			Log.info(path);
		}

		return result;

	}

	/**
	 * Locate a vertex inside the graph
	 * 
	 * @param name
	 *            vertex name
	 * @param graph
	 *            input graph
	 * @return target vertex
	 */
	public Vertex getVertex(String name, GraphJung<Graph> graph) {

		Collection<Vertex> vertices = graph.getVertices();
		Vertex vertexResult = null;

		for (Iterator<Vertex> iterator = vertices.iterator(); iterator
				.hasNext();) {
			Vertex vertex = (Vertex) iterator.next();
			if (vertex.getId().equals(name))
				vertexResult = vertex;

		}
		return vertexResult;

	}

	/*
	 * Vertex current_vertex = start_vertex;
	 * 
	 * System.out.println("Input Vertices List:"); for (Iterator<Vertex>
	 * in_vertices = current_vertex.getVertices( Direction.IN).iterator();
	 * in_vertices.hasNext();) { System.out.println(in_vertices.next()); }
	 * 
	 * System.out.println("Output Vertices List:"); for (Iterator<Vertex>
	 * out_vertices = current_vertex.getVertices( Direction.OUT).iterator();
	 * out_vertices.hasNext();) { System.out.println(out_vertices.next()); }
	 * 
	 * System.out.println("Both Vertices List:"); for (Iterator<Vertex>
	 * both_vertices = current_vertex.getVertices( Direction.BOTH).iterator();
	 * both_vertices.hasNext();) { System.out.println(both_vertices.next()); }
	 * 
	 * current_vertex.getVertices(Direction.OUT);
	 * 
	 * while (!new_nodes_queue.isEmpty()) { } }
	 */

	/* Program first-depth algorithm!! For several paths! */

	/**
	 * @return the nodesPath
	 */
	public ArrayList<Vertex> getNodesPath() {
		return nodesPath;
	}

	/**
	 * @param nodesPath
	 *            the nodesPath to set
	 */
	public void setNodesPath(ArrayList<Vertex> nodesPath) {
		this.nodesPath = nodesPath;
	}

	/**
	 * @return the proteinsPath
	 */
	public ArrayList<Vertex> getProteinsPath() {
		return proteinsPath;
	}

	/**
	 * @param proteinsPath
	 *            the proteinsPath to set
	 */
	public void setProteinsPath(ArrayList<Vertex> proteinsPath) {
		this.proteinsPath = proteinsPath;
	}

	public Graph createTestGraph() {
		Graph graph = new TinkerGraph();

		Vertex vertex_1 = graph.addVertex("1");
		Vertex vertex_2 = graph.addVertex("2");
		Vertex vertex_3 = graph.addVertex("3");
		Vertex vertex_4 = graph.addVertex("4");
		Vertex vertex_5 = graph.addVertex("5");
		Vertex vertex_6 = graph.addVertex("6");
		Vertex vertex_7 = graph.addVertex("7");
		Vertex vertex_8 = graph.addVertex("8");

		graph.addEdge("e1", vertex_1, vertex_2, "");
		graph.addEdge("e2", vertex_2, vertex_3, "");
		graph.addEdge("e3", vertex_2, vertex_4, "");
		graph.addEdge("e4", vertex_2, vertex_6, "");
		graph.addEdge("e5", vertex_3, vertex_7, "");
		graph.addEdge("e6", vertex_4, vertex_5, "");
		graph.addEdge("e7", vertex_6, vertex_5, "");
		graph.addEdge("e8", vertex_6, vertex_7, "");
		graph.addEdge("e9", vertex_6, vertex_8, "");
		graph.addEdge("e10", vertex_5, vertex_1, "");
		graph.addEdge("e11", vertex_7, vertex_1, "");

		return graph;

	}

	public boolean travelGraph(String start, String end, Graph graph, int depth) {
		
		System.out.println("");
		System.out.println("Depth: "+depth);

		System.out.println("Traveling the graph..");
		boolean error = false;
		int count = 0;

		if (graph == null) {
			error = true;
		} else {
			Vertex start_vertex = graph.getVertex(start);
			Vertex end_vertex = graph.getVertex(end);

			// Validates inputs
			if (start_vertex == null) {
				System.out.println("Start node does not exist!");
				error = true;
			}

			if (end_vertex == null) {
				error = true;
				System.out.println("End node does not exist!");
			}

			if (!error) {
				LinkedList<Path> paths_queue = new LinkedList<Path>();
				// ArrayList<Path> archive_queue = new ArrayList<Path>();
				ArrayList<Path> result_queue = new ArrayList<Path>();

				paths_queue.add(new Path(start_vertex));
				count++;

				while (paths_queue.peek() != null) {

					Path path = paths_queue.pollFirst();
					Vertex last = path.getLast();
					Iterable<Vertex> successors = last
							.getVertices(Direction.OUT);

					// If the path is longer than the defined depth stop
					if (path.size() >= depth) {
						//path.setDescription("The algorithm reached its maximum depth.");
						// archive_queue.add(path);
						continue;
					}

					if (!successors.iterator().hasNext()) {
						//path.setDescription("There are no more nodes to analyze, successors are null.");
						// archive_queue.add(path);
						continue;
					}

					// For each successor
					for (Iterator<Vertex> list = successors.iterator(); list
							.hasNext();) {

						// Get next node
						Vertex next_succ = list.next();
						Path newPath = new Path(path);
						count++;
						boolean add_node = newPath.add(next_succ);

						// Did we met the goal?
						if (next_succ.getId().equals(end_vertex.getId())) {
							//newPath.setDescription("Goal reached");
							result_queue.add(newPath);
						}
						// Are we trapped in a loop?
						else if (!add_node) {
							//newPath.setDescription("There is a loop");
							// archive_queue.add(newPath);

						}
						// No? Add it again to the queue+
						else {
							paths_queue.add(newPath);
						}
					}
				}

				//System.out.println("\nPaths Queue:" + paths_queue.size());
				System.out.println("Result Queue:" + result_queue.size());
				System.out.println("");
				for (Path path : result_queue) {
					path.print();
				}
				System.out.println("Paths Found:" + result_queue.size());
				System.out.println("Paths analyzed: "+count);
				// System.out.println("Archive Queue:" + archive_queue.size());
				// for (Path path : archive_queue) {
				// path.print();

				// }
			}

		}

		System.out.println("Done: " + error);
		return error;
	}

	public static void main(String[] args) {

		NavigateGraph navigator = new NavigateGraph();
		Graph graph = navigator.createTestGraph();
		// navigator.printGraph(graph);

		navigator.travelGraph("6", "1", graph,0);

	}

	public void printGraph(Graph graph) {

		System.out.println("");
		System.out.println("Printing graph..");
		System.out.println("List of vertices: ");

		for (Vertex vertex : graph.getVertices()) {

			System.out.println("Vertex:" + vertex);
			for (String item : vertex.getPropertyKeys()) {
				System.out.println(item + ": " + vertex.getProperty(item));
			}
			System.out.println("Vertices IN direction: "
					+ vertex.getEdges(Direction.IN));
			System.out.println("Vertices OUT direction: "
					+ vertex.getEdges(Direction.OUT));
			System.out.println("");

		}

		System.out.println("List of edges: ");
		for (Edge edge : graph.getEdges()) {

			System.out.println("Edge:" + edge);
			for (String item : edge.getPropertyKeys()) {
				System.out.println(item + ": " + edge.getProperty(item));
			}
			System.out.println("Edges IN direction: "
					+ edge.getVertex(Direction.IN));
			System.out.println("Edges OUT direction: "
					+ edge.getVertex(Direction.OUT));
			System.out.println(" ");
		}
	}

}
