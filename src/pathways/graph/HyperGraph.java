package pathways.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import pathways.datautils.Log;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.tg.TinkerGraph;

public class HyperGraph {

	private Graph hyperGraph;
	private Hashtable<String, Vertex> verticesHash;
	private Hashtable<String, Edge> edgesHash;
	private ArrayList<Graph> graphList;
	private Hashtable<String, String> graphIdsDictionary;

	/**
	 * @param hyperGraph global hyper graph
	 * @param verticesHash hash unique of vertices
	 * @param edgesHash hash unique edges
	 * @param graphList list of graphs analyzed
	 * @param graphIdsDictionary dictionary that keeps the parent of each node
	 */
	public HyperGraph() {
		super();
		this.hyperGraph = new TinkerGraph(); 
		this.verticesHash = new Hashtable<String, Vertex>();
		this.edgesHash = new Hashtable<String, Edge>();
		this.graphList = new ArrayList<Graph>();
		this.graphIdsDictionary = new Hashtable<String, String>();
	}

	/**
	 * Add a graph to the hypergraph
	 * 
	 * @param graph
	 *            input graph
	 * @param parentMap
	 *            name of the file it came from
	 */
	public void addGraph(Graph graph, String parentMap) {

		Log.info("Merging graph " + parentMap + "..");
		List<Vertex> vertices = (List<Vertex>) graph.getVertices();
		List<Edge> edges = (List<Edge>) graph.getEdges();

		// Keep a reference to the original graph
		graphList.add(graph);
		graphIdsDictionary.put(graph.hashCode() + "", parentMap);

		// Add each vertex
		for (Vertex vertex : vertices) {

			String keggsIds = vertex.getProperty("keggIds");

			// Ignore null vertices
			if (keggsIds == null) {
				System.out.println("There is a null vertex!" + vertex);
				continue;
			}

			String key = createKey(keggsIds.toString());

			// Do not repeat any vertex
			if (!verticesHash.containsKey(key)) {

				// Create a new vertex
				Vertex newVertex = hyperGraph.addVertex(key);

				// Import all its properties from the old node
				Set<String> properties = vertex.getPropertyKeys();

				for (String property : properties) {
					newVertex.setProperty(property,
							vertex.getProperty(property));
					// Reset position
					newVertex.setProperty("nodePosition", "0|0");
				}

				// Set Parent
				newVertex.setProperty("parent", graph.hashCode() + "");
				verticesHash.put(key, newVertex);
			}

		}

		// Add each edge
		for (Edge edge : edges) {

			// Head Vertex
			Vertex inVertex = edge.getVertex(Direction.IN);
			String inVertexKey = createKey(inVertex.getProperty("keggIds")
					.toString());

			// Tail Vertex
			Vertex outVertex = edge.getVertex(Direction.OUT);
			String outVertexKey = createKey(outVertex.getProperty("keggIds")
					.toString());

			String edgeKey = outVertexKey + "-" + inVertexKey;

			// New Edge (out-in)
			if (!edgesHash.containsKey(edgeKey)) {

				Vertex newinVertex = verticesHash.get(inVertexKey);
				Vertex newoutVertex = verticesHash.get(outVertexKey);

				Edge newEdge = hyperGraph.addEdge(edgeKey, newoutVertex,
						newinVertex, "key");

				newEdge.setProperty("key", edgeKey);
				edgesHash.put(edgeKey, newEdge);

			}
		}
		printHyperGraph();

	}

	/**
	 * Create a key from the IDs in KeggsIds property
	 * 
	 * @param listIds
	 *            List of ids
	 * @return key for edges and vertices hash
	 */
	private String createKey(String listIds) {

		String key = "";
		String[] keyList = listIds.split(",");
		Arrays.sort(keyList);

		for (String item : keyList)
			key += item.trim() + "+";

		return key.substring(0, key.length() - 1);

	}

	/**
	 * Print Hyper graph with GraphIO method
	 */
	public void printHyperGraph() {
		GraphIO graph = new GraphIO();
		graph.printGraph(hyperGraph);
	}

	/**
	 * @return the hyperGraph
	 */
	public Graph getHyperGraph() {
		return hyperGraph;
	}

	/**
	 * @param hyperGraph
	 *            the hyperGraph to set
	 */
	public void setHyperGraph(Graph hyperGraph) {
		this.hyperGraph = hyperGraph;
	}

	/**
	 * @return the verticesHash
	 */
	public Hashtable<String, Vertex> getVerticesHash() {
		return verticesHash;
	}

	/**
	 * @param verticesHash
	 *            the verticesHash to set
	 */
	public void setVerticesHash(Hashtable<String, Vertex> verticesHash) {
		this.verticesHash = verticesHash;
	}

	/**
	 * @return the edgesHash
	 */
	public Hashtable<String, Edge> getEdgesHash() {
		return edgesHash;
	}

	/**
	 * @param edgesHash
	 *            the edgesHash to set
	 */
	public void setEdgesHash(Hashtable<String, Edge> edgesHash) {
		this.edgesHash = edgesHash;
	}

	/**
	 * @return the graphList
	 */
	public ArrayList<Graph> getGraphList() {
		return graphList;
	}

	/**
	 * @param graphList
	 *            the graphList to set
	 */
	public void setGraphList(ArrayList<Graph> graphList) {
		this.graphList = graphList;
	}

	/**
	 * @return the graphIdsDictionary
	 */
	public Hashtable<String, String> getGraphIdsDictionary() {
		return graphIdsDictionary;
	}

	/**
	 * @param graphIdsDictionary
	 *            the graphIdsDictionary to set
	 */
	public void setGraphIdsDictionary(
			Hashtable<String, String> graphIdsDictionary) {
		this.graphIdsDictionary = graphIdsDictionary;
	}

}
