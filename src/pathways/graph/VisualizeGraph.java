package pathways.graph;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.apache.commons.collections15.Transformer;

import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.oupls.jung.GraphJung;

import edu.uci.ics.jung.algorithms.layout.FRLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.visualization.BasicVisualizationServer;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;

public class VisualizeGraph {

	public VisualizeGraph() {
	}

	public void showGraph(Graph inputGraph) {

		GraphJung<Graph> graph = new GraphJung<Graph>(inputGraph);

		// Layouts:
		// http://jung.sourceforge.net/site/apidocs/edu/uci/ics/jung/algorithms/layout/package-summary.html
		Layout<Vertex, Edge> layout = new FRLayout<Vertex, Edge>(graph);
		// Layout<Vertex, Edge> layout = new SpringLayout<Vertex, Edge>(graph);
		layout.setSize(new Dimension(1366, 768));
		BasicVisualizationServer<Vertex, Edge> viz = new BasicVisualizationServer<Vertex, Edge>(
				layout);
		viz.setPreferredSize(new Dimension(1366, 768));

		Transformer<Vertex, String> vertexLabelTransformer = new Transformer<Vertex, String>() {
			public String transform(Vertex vertex) {
				return (String) vertex.getProperty("nodeName");
			}
		};

		Transformer<Edge, String> edgeLabelTransformer = new Transformer<Edge, String>() {
			public String transform(Edge edge) {
				return (String) edge.getProperty("id");
			}
		};

		viz.getRenderContext().setEdgeLabelTransformer(edgeLabelTransformer);
		viz.getRenderContext()
				.setVertexLabelTransformer(vertexLabelTransformer);

		/*
		 * Look and Feel
		 */

		/*
		 * Transformer<Integer, Paint> vertexPaint = new Transformer<Integer,
		 * Paint>() { public Paint transform(Integer i) { return Color.GREEN; }
		 * };
		 */
		// Set up a new stroke Transformer for the edges
		/*
		 * float dash[] = { 10.0f }; final Stroke edgeStroke = new
		 * BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER,
		 * 10.0f, dash, 0.0f); Transformer<String, Stroke> edgeStrokeTransformer
		 * = new Transformer<String, Stroke>() { public Stroke transform(String
		 * s) { return edgeStroke; } };
		 */

		/*
		 * viz.getRenderContext().setVertexDrawPaintTransformer(vertexPaint);
		 * viz
		 * .getRenderContext().setEdgeStrokeTransformer(edgeStrokeTransformer);
		 * viz.getRenderContext() .setVertexLabelTransformer(new
		 * ToStringLabeller());
		 * viz.getRenderContext().setEdgeLabelTransformer(new
		 * ToStringLabeller());
		 * viz.getRenderer().getVertexLabelRenderer().setPosition
		 * (Position.CNTR);
		 */

		JFrame frame = new JFrame("TinkerPop");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel container = new JPanel();
		JScrollPane srcPane = new JScrollPane(container);
		container.add(viz);

		// JScrollPane scroll = new JScrollPane(viz);
		// scroll.add(viz);

		frame.getContentPane().add(srcPane);
		// viz.createImage(2000, 2000);
		frame.pack();

		frame.setVisible(true);
	}

	public void showInteractiveGraph(Graph inputGraph) {

		// I create the graph in the following...
		GraphJung<Graph> graph = new GraphJung<Graph>(inputGraph);
		// Layout<V, E>, VisualizationComponent<V,E>
		Layout<Vertex, Edge> layout = new FRLayout<Vertex, Edge>(graph);
		// Layout<Integer, String> layout = new CircleLayout(graph);

		layout.setSize(new Dimension(1366, 768));
		// VisualizationViewer<Integer, String> vv = new
		// VisualizationViewer<Integer, String>(layout);
		VisualizationViewer<Vertex, Edge> vv = new VisualizationViewer<Vertex, Edge>(
				layout);

		vv.setPreferredSize(new Dimension(350, 350));
		// Show vertex and edge labels
		//vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
		//vv.getRenderContext().setEdgeLabelTransformer(new ToStringLabeller());
		// Create a graph mouse and add it to the visualization component
		DefaultModalGraphMouse<Object, Object> gm = new DefaultModalGraphMouse<Object, Object>();
		gm.setMode(ModalGraphMouse.Mode.TRANSFORMING);
		vv.setGraphMouse(gm);
		JFrame frame = new JFrame("Interactive Graph View 1");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(vv);
		frame.pack();
		frame.setSize(new Dimension(1366, 768));
		frame.setVisible(true);

	}

}
