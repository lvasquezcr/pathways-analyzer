package pathways.graph;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pathways.config.ConfigData;
import pathways.datautils.ExternalCmdsManager;
import pathways.datautils.Log;
import pathways.entities.ExecResult;
import pathways.translator.TranslateKGML;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.tg.TinkerGraph;
import com.tinkerpop.blueprints.util.io.graphml.GraphMLReader;

public class GraphIO {

	/***
	 * Sets a flag to remove or not nested graphs
	 */

	public GraphIO() {

	}

	/***
	 * Reads a graph into blueprints graph from a .graphml file
	 * 
	 * @param filename
	 *            input file name
	 * @return blueprints graph
	 * @throws IOException
	 *             Source:
	 *             https://github.com/tinkerpop/blueprints/wiki/GraphML-Reader
	 *             -and-Writer-Library API:
	 *             http://www.tinkerpop.com/docs/javadocs/blueprints/2.4.0/
	 */
	public Graph readGraph(String filename) throws IOException {

		Graph graph = null;

		Log.info("Loading file " + filename + "..");

		graph = new TinkerGraph();
		InputStream in = new FileInputStream(new File(filename));
		GraphMLReader reader = new GraphMLReader(graph);

		reader.setVertexIdKey("_id");
		reader.setEdgeIdKey("_id");
		reader.setEdgeLabelKey("_label");

		try {
			reader.inputGraph(in);

		} catch (IllegalArgumentException exception) {
			Log.warning("Graph " + filename
					+ " may have nested graphs. Fixing this graph..");
			removeNestedGraphs(filename);
			reader.inputGraph(in);

		}

		Log.info("Graph loaded successfully => " + graph);

		return graph;

	}

	/***
	 * Prints all graph attributes
	 * 
	 * @param graph
	 *            input graph to print
	 */
	public void printGraph(Graph graph) {

		Log.debug("");
		Log.debug("Printing graph..");
		Log.debug("List of vertices: ");

		for (Vertex vertex : graph.getVertices()) {

			Log.debug("Vertex:" + vertex);
			for (String item : vertex.getPropertyKeys()) {
				Log.debug(item + ": " + vertex.getProperty(item));
			}
			Log.debug("Vertices IN direction: " + vertex.getEdges(Direction.IN));
			Log.debug("Vertices OUT direction: "
					+ vertex.getEdges(Direction.OUT));
			Log.debug("");

		}

		Log.debug("List of edges: ");
		for (Edge edge : graph.getEdges()) {

			Log.debug("Edge:" + edge);
			for (String item : edge.getPropertyKeys()) {
				Log.debug(item + ": " + edge.getProperty(item));
			}
			Log.debug("Edges IN direction: " + edge.getVertex(Direction.IN));
			Log.debug("Edges OUT direction: " + edge.getVertex(Direction.OUT));
			Log.debug(" ");
		}
	}

	/***
	 * Removes nested graphs from .graphml files
	 * 
	 * @param file
	 *            input filename
	 * @return
	 * @throws IOException
	 */
	public ExecResult removeNestedGraphs(String file) throws IOException {

		ExecResult result = null;
		List<String> cmd = Arrays
				.asList(ConfigData.fixNestedGraphsScript, file);

		try {
			result = ExternalCmdsManager.exec(cmd);

			if (result.isFailed()) {
				Log.error("./fixGraph.rb failed fixing the input file " + file);
				Log.error("Details:");
				Log.error(result.getError());

			}
		} catch (InterruptedException e) {
			Log.error("./fixGraph.rb failed fixing the input file " + file);
			Log.error("Details: " + e.getMessage());
		}

		return result;

	}

	/***
	 * Translate graphs from KEGG format to .graphml with KEGG translator
	 * 
	 * @param pathways
	 *            list of KEGG files
	 * @return hyper graph with the translated pathways
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws Exception
	 */
	public HyperGraph translateGraphs(ArrayList<String> pathways)
			throws IOException, InterruptedException {

		Log.info("Translating " + pathways.size() + " pathways..");

		ArrayList<Graph> graphs = new ArrayList<Graph>();
		ArrayList<String> graphParents = new ArrayList<String>();
		TranslateKGML translator = new TranslateKGML();
		boolean error = false;

		for (int i = 0; i < pathways.size(); i++) {

			String pathFile = pathways.get(i);
			Log.info("Parsing pathway " + pathFile + "..");

			String graphmlFilename = ConfigData.graphmlDir + pathFile
					+ ".graphml";
			File graphmlFile = new File(graphmlFilename);

			String sourceFilename = ConfigData.pathwaysDir + pathFile + ".xml";
			File sourceFile = new File(sourceFilename);

			String destinationFilename = ConfigData.kgmlDir + pathFile + ".xml";
			File destinationFile = new File(destinationFilename);

			// Check if the .graphml file already exist
			if (!graphmlFile.exists()) {

				Log.warning("File "
						+ graphmlFilename
						+ " is not available, it will be translated from source stored in "
						+ ConfigData.pathwaysDir);

				// Check if the source .kgml is already available locally
				if (!destinationFile.exists()) {

					// Check the source .kgml file is a valid destination
					if (sourceFile.exists()) {
						Files.copy(sourceFile.toPath(),
								destinationFile.toPath(),
								StandardCopyOption.REPLACE_EXISTING);
					} else {
						Log.error("Source file for " + sourceFilename
								+ " does not exist.");
						error = true;

					}
				}

				if (!error)
					error = translator.translate2GraphML(sourceFilename);

			} else {
				Log.info("No translation required for: " + graphmlFilename);
			}

			if (!error) {
				Graph graph = readGraph(graphmlFilename);
				if (graph == null) {
					Log.error("Error loading graph " + pathFile
							+ ", graph is null");
				} else {
					printGraph(graph);
					graphs.add(graph);
					graphParents.add(graphmlFile.getName());
				}
			}

		}

		return mergeGraphs(graphs, graphParents);

	}

	/***
	 * Merge all the translated pathways
	 * 
	 * @param graphs
	 *            list of available graphs to merge
	 * @param graphParents
	 *            list of files where the graphs come from (parent .kgml)
	 * @return graphs merged into an Hypergraph
	 */
	public HyperGraph mergeGraphs(ArrayList<Graph> graphs,
			ArrayList<String> graphParents) {

		HyperGraph hyperGraph = new HyperGraph();

		for (int i = 0; i < graphs.size(); i++) {
			hyperGraph.addGraph(graphs.get(i), graphParents.get(i));

		}

		return hyperGraph;
	}

}
