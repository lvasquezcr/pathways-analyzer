package pathways.config;

public class ConfigData {

	public static String homeDirectory = System.getProperty("user.home");
	public static String workingDirectory = System.getProperty("user.dir");

	public static String pathwaysDir = workingDirectory + "/data/";
	public static String pathwaysFile = workingDirectory + "/data/graphml/list";
	public static String biobricksFile = workingDirectory
			+ "/data/biobricks/biosynthesis/parts/list/";

	public static String fixNestedGraphsScript = "./lib/fixGraph.rb";
	public static String queryGenBankScript = "./lib/getData.pl";

	public static boolean DEBUG = false;

	public static String[][] outputCompounds = { { "cpd:C00022", "Pyruvate" },
			{ "cpd:C00024", "Acetyl-CoA" }

	};

	public static String[][] inputCompounds = { { "cpd:C00243", "Lactose" },
			{ "cpd:C00760", "Cellulose" }, { "cpd:C00461", "Chitin" },
			{ "cpd:C00461", "Collagen" },
			{ "cpd:C15804", "p-Hydroxyphenyl lignin" },
			{ "cpd:C15805", "Guaiacyl lignin" },
			{ "cpd:C15807", "5-Hydroxy-guaiacyl lignin" },
			{ "cpd:C15806", "Syringyl lignin" }, { "", "Collagen" } };
	
	
	// Grapml directory
	public static String graphmlDir = "data/graphml/";
	public static String kgmlDir = "data/kgml/";
	public static String cdsDir = "data/cds/";
	

}
